FROM python:2
COPY requirements.txt /
RUN pip install -r /requirements.txt && rm /requirements.txt
WORKDIR /go
ENTRYPOINT ["python", "go.py"]
COPY ["css", "css/"]
COPY ["js", "js/"]
COPY ["html", "html/"]
COPY ["go.py", "go.cfg", "robots.txt", "LICENSE.txt", "TERMS_OF_USE.txt", "./"]
